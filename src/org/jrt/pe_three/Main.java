/**
 * org.jrt.pe_three is a list of operations to complete project euler problem three
 */
package org.jrt.pe_three;

import java.math.BigInteger;

/**
 * The prime factors of 13195 are 5, 7, 13 and 29.
 * 
 * What is the largest prime factor of the number 600851475143 ?
 * 
 * @author jrtobac
 *
 */
public class Main {

	public static void main(String[] args) {	
		
		BigInteger num = new BigInteger("600851475143");
		BigInteger primeNum = new BigInteger("2");
		final BigInteger ZERO = new BigInteger("0");
		
		while(primeNum.compareTo(num) < 0) {
			if(num.mod(primeNum).equals(ZERO)) {
				num = num.divide(primeNum);
			}else {
				primeNum = primeNum.nextProbablePrime();
			}
		}
		
		System.out.println(num);
	}
}
